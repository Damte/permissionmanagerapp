This is the repository where it is possible to find the backend corresponding to the task that was sent to me via e-mail on 18th of December.

The corresponding frontend has been added in a separate repository:
https://bitbucket.org/Damte/permissionmanagerappui/src/master/

For the backend I used mostly spring boot, with JPA and MySQL for setting the database.

I managed a data model that was meant to take care of the task provided as I understood it, perhaps in hindsight I would have used self-referencing for handling all the permissions instead of separate tables, but as it was I have rather developed what I had. My other major concern is that due to time and schedule limitations I didn't get to implement JUnit testing.

On the frontend side, I managed to implement some simple interface as well as the basic functionality of the task. There are some bugs and improvements of which I'm aware of, but again I lacked the time to properly address.