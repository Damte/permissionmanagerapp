package com.cleveron.permission.manager.demo.controller;

import com.cleveron.permission.manager.demo.dto.TertiaryPermissionDTO;
import com.cleveron.permission.manager.demo.entity.TertiaryPermission;
import com.cleveron.permission.manager.demo.exception.MissingParentPermission;
import com.cleveron.permission.manager.demo.service.TertiaryPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class TertiaryPermissionController {

    private TertiaryPermissionService tertiaryPermissionService;

    @Autowired
    public TertiaryPermissionController(TertiaryPermissionService tertiaryPermissionService) {
        this.tertiaryPermissionService = tertiaryPermissionService;
    }

    @GetMapping(value = "/tertiary-permissions")
    public ResponseEntity<List<TertiaryPermission>> findAllPrimaryPermissions() {
        List<TertiaryPermission> tertiaryPermissions = tertiaryPermissionService.findAll();
        return new ResponseEntity<>(tertiaryPermissions, HttpStatus.OK);
    }

    @PatchMapping(value = "/tertiary-permission/{id}")
    public ResponseEntity<TertiaryPermission> togglePermissionIsActive(@PathVariable("id") Long permissionId, @RequestBody TertiaryPermissionDTO tertiaryPermissionDTO) {
        try {
            TertiaryPermission updatePermission = tertiaryPermissionService.togglePermissionIsActive(permissionId, tertiaryPermissionDTO);
            return new ResponseEntity<>(updatePermission, HttpStatus.ACCEPTED);
        } catch (MissingParentPermission e) {
            return new ResponseEntity(e, HttpStatus.CONFLICT);
        }
    }
}
