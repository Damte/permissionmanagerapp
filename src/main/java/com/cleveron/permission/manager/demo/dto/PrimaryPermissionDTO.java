package com.cleveron.permission.manager.demo.dto;

public class PrimaryPermissionDTO {
    private String name;
    private Long primaryPermissionId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrimaryPermissionId() {
        return primaryPermissionId;
    }

    public void setPrimaryPermissionId(Long primaryPermissionId) {
        this.primaryPermissionId = primaryPermissionId;
    }
}
