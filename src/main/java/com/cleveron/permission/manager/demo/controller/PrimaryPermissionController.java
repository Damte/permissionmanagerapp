package com.cleveron.permission.manager.demo.controller;

import com.cleveron.permission.manager.demo.entity.PrimaryPermission;
import com.cleveron.permission.manager.demo.service.PrimaryPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class PrimaryPermissionController {

    private PrimaryPermissionService primaryPermissionService;

    @Autowired
    public PrimaryPermissionController(PrimaryPermissionService primaryPermissionService) {
        this.primaryPermissionService = primaryPermissionService;
    }

    @GetMapping(value = "/primary-permissions")
    public ResponseEntity<List<PrimaryPermission>> findAllPrimaryPermissions() {
        List<PrimaryPermission> primaryPermissions = primaryPermissionService.findAll();
        return new ResponseEntity<>(primaryPermissions, HttpStatus.OK);
    }


//    @GetMapping(value = "role/{role-id}/primary-permissions")
//    public ResponseEntity<Set<PrimaryPermission>> getPermissionsByRole(@PathVariable("role-id") Long roleId) {
//        try {
//            if (primaryPermissionService.checkRoleExistence(roleId)) {
//                Set<PrimaryPermission> primaryPermissions = primaryPermissionService.findPermissionsByRoleId(roleId);
//                return new ResponseEntity(primaryPermissions, HttpStatus.FOUND);
//            } else {
//                throw new RoleDoesntExist("There is no role with Id: " + roleId);
//            }
//        } catch (RoleDoesntExist e) {
//            return new ResponseEntity(e, HttpStatus.NOT_FOUND);
//        }
//    }
}
