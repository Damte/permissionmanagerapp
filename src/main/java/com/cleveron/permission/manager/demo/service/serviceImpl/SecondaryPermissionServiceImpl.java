package com.cleveron.permission.manager.demo.service.serviceImpl;

import com.cleveron.permission.manager.demo.dto.SecondaryPermissionDTO;
import com.cleveron.permission.manager.demo.entity.SecondaryPermission;
import com.cleveron.permission.manager.demo.exception.MissingParentPermission;
import com.cleveron.permission.manager.demo.repository.SecondaryPermissionRepository;
import com.cleveron.permission.manager.demo.service.PrimaryPermissionService;
import com.cleveron.permission.manager.demo.service.SecondaryPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SecondaryPermissionServiceImpl implements SecondaryPermissionService {

    private final SecondaryPermissionRepository secondaryPermissionRepository;
    private final PrimaryPermissionService primaryPermissionService;

    @Autowired
    public SecondaryPermissionServiceImpl(SecondaryPermissionRepository secondaryPermissionRepository, PrimaryPermissionService primaryPermissionService) {
        this.secondaryPermissionRepository = secondaryPermissionRepository;
        this.primaryPermissionService = primaryPermissionService;
    }

    @Override
    public SecondaryPermission findById(Long id) {
        return secondaryPermissionRepository.findById(id).get();
    }


    @Override
    public SecondaryPermission saveSecondaryPermission(SecondaryPermission secondaryPermission) {
        return secondaryPermissionRepository.save(secondaryPermission);
    }

    @Override
    public List<SecondaryPermission> findAllInactive() {

        return secondaryPermissionRepository.findAllByIsActiveFalse();
    }

    @Override
    public SecondaryPermission togglePermissionIsActive(Long permissionId, SecondaryPermissionDTO secondaryPermissionDTO) throws MissingParentPermission {
        SecondaryPermission permissionToBeUpdated = secondaryPermissionRepository.findById(permissionId).get();
        if (primaryPermissionService.findById(secondaryPermissionDTO.getPrimaryPermissionId()).equals(permissionToBeUpdated.getPrimaryPermission())) {
            permissionToBeUpdated.setActive(secondaryPermissionDTO.getActive());
            return secondaryPermissionRepository.save(permissionToBeUpdated);
        } else {
            throw new MissingParentPermission("Please add the parent permission first: " + permissionToBeUpdated.getPrimaryPermission().getName());
        }
    }


}
