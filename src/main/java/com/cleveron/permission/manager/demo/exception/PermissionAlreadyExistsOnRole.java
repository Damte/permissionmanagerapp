package com.cleveron.permission.manager.demo.exception;

public class PermissionAlreadyExistsOnRole extends Throwable {
    public PermissionAlreadyExistsOnRole(String s) {
        super(s);
    }

}
