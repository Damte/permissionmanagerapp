package com.cleveron.permission.manager.demo.service.serviceImpl;

import com.cleveron.permission.manager.demo.entity.PrimaryPermission;
import com.cleveron.permission.manager.demo.repository.PrimaryPermissionRepository;
import com.cleveron.permission.manager.demo.service.PrimaryPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrimaryPermissionServiceImpl implements PrimaryPermissionService {

    private final PrimaryPermissionRepository primaryPermissionRepository;
    //    private final RoleRepository roleRepository;

    @Autowired
    public PrimaryPermissionServiceImpl(PrimaryPermissionRepository primaryPermissionRepository) {
        this.primaryPermissionRepository = primaryPermissionRepository;
    }

    @Override
    public PrimaryPermission savePermission(PrimaryPermission primaryPermission) {
        return primaryPermissionRepository.save(primaryPermission);
    }

    @Override
    public List<PrimaryPermission> findAll() {
        return primaryPermissionRepository.findAll();
    }

    @Override
    public PrimaryPermission findById(Long id) {
        return primaryPermissionRepository.findById(id).get();
    }


}
