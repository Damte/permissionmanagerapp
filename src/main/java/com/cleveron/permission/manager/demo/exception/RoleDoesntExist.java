package com.cleveron.permission.manager.demo.exception;

public class RoleDoesntExist extends Throwable {
    public RoleDoesntExist(String s) {
        super(s);
    }

}
