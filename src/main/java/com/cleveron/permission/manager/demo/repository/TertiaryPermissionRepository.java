package com.cleveron.permission.manager.demo.repository;

import com.cleveron.permission.manager.demo.entity.TertiaryPermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TertiaryPermissionRepository extends JpaRepository<TertiaryPermission, Long> {
}
