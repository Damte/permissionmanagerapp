package com.cleveron.permission.manager.demo.service.serviceImpl;

import com.cleveron.permission.manager.demo.dto.PrimaryPermissionDTO;
import com.cleveron.permission.manager.demo.entity.PrimaryPermission;
import com.cleveron.permission.manager.demo.entity.Role;
import com.cleveron.permission.manager.demo.exception.PermissionAlreadyExistsOnRole;
import com.cleveron.permission.manager.demo.exception.PermissionNotIncludedInRole;
import com.cleveron.permission.manager.demo.repository.RoleRepository;
import com.cleveron.permission.manager.demo.service.PrimaryPermissionService;
import com.cleveron.permission.manager.demo.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;
    private final PrimaryPermissionService primaryPermissionService;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository, PrimaryPermissionService primaryPermissionService) {
        this.roleRepository = roleRepository;
        this.primaryPermissionService = primaryPermissionService;
    }

    @Override
    public Role updatePermission(Long id, List<PrimaryPermission> primaryPermissionList) {
        Role role = roleRepository.getOne(id);
        role.setPermissions(primaryPermissionList);
        Role updatedRole = roleRepository.save(role);
        return updatedRole;
    }

    @Override
    public List<PrimaryPermission> addNewPermission(Long id, PrimaryPermissionDTO primaryPermissionDTO) throws PermissionAlreadyExistsOnRole {
        Role role = roleRepository.getOne(id);
        List<PrimaryPermission> existingPermissions = role.getPermissions();
        PrimaryPermission primaryPermission = primaryPermissionService.findById(primaryPermissionDTO.getPrimaryPermissionId());
        if (!existingPermissions.contains(primaryPermission)) {
            existingPermissions.add(primaryPermission);
            roleRepository.save(role);
        } else {
            throw new PermissionAlreadyExistsOnRole(primaryPermissionDTO.getName() + " it is already included in " + role.getName());
        }
        return existingPermissions;
    }

    @Override
    public List<PrimaryPermission> removeExistingPermission(Long id, PrimaryPermissionDTO primaryPermissionDTO) throws PermissionNotIncludedInRole {
        Role role = roleRepository.findById(id).get();
        PrimaryPermission primaryPermission = primaryPermissionService.findById(primaryPermissionDTO.getPrimaryPermissionId());
        List<PrimaryPermission> existingPermissions = role.getPermissions();
        if (existingPermissions.contains(primaryPermission)) {
            int index = existingPermissions.indexOf(primaryPermission);
            existingPermissions.remove(index);
            roleRepository.save(role);
        } else {
            throw new PermissionNotIncludedInRole(primaryPermissionDTO.getName() + " does not belong to " + role.getName());
        }
        return existingPermissions;
    }


    @Override
    public List<Role> findAllRoles() {
        return roleRepository.findAll();
    }

    @Override
    public Role findRole(Long roleId) {
        return roleRepository.findById(roleId).get();
    }

    @Override
    public Role saveRole(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public Role updateRole(Role role) {
        List<PrimaryPermission> primaryPermissions = role.getPermissions();
        primaryPermissions.stream().forEach(permission -> primaryPermissionService.savePermission(permission));
        return roleRepository.save(role);
    }

    @Override
    public Boolean checkRoleExistence(Long roleId) {
        return roleRepository.existsById(roleId);
    }

    @Override
    public List<PrimaryPermission> findPermissionsByRoleId(Long roleId) {
        Role role = roleRepository.findById(roleId).orElseThrow();
        List<PrimaryPermission> primaryPermissions = role.getPermissions();
        return primaryPermissions;
    }
}
