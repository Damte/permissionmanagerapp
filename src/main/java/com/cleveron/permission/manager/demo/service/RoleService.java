package com.cleveron.permission.manager.demo.service;

import com.cleveron.permission.manager.demo.dto.PrimaryPermissionDTO;
import com.cleveron.permission.manager.demo.entity.PrimaryPermission;
import com.cleveron.permission.manager.demo.entity.Role;
import com.cleveron.permission.manager.demo.exception.PermissionAlreadyExistsOnRole;
import com.cleveron.permission.manager.demo.exception.PermissionNotIncludedInRole;

import java.util.List;

public interface RoleService {

    Role updatePermission(Long id, List<PrimaryPermission> primaryPermissionSet);

    List<PrimaryPermission> addNewPermission(Long id, PrimaryPermissionDTO primaryPermissionDTO) throws PermissionAlreadyExistsOnRole;

    List<PrimaryPermission> removeExistingPermission(Long id, PrimaryPermissionDTO primaryPermissionDTO) throws PermissionNotIncludedInRole;

    List<Role> findAllRoles();

    Role findRole(Long roleId);

    Role saveRole(Role role);

    Role updateRole(Role role);

    Boolean checkRoleExistence(Long roleId);

    List<PrimaryPermission> findPermissionsByRoleId(Long roleId);

}
