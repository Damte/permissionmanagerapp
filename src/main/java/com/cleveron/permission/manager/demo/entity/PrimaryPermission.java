package com.cleveron.permission.manager.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity(name = "permission")
public class PrimaryPermission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long entityId;

    @Column(name = "NAME")
    private String name;

    @JsonIgnore
    @ManyToMany(mappedBy = "primaryPermissions")
    private List<Role> roles;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "primaryPermission", cascade = CascadeType.ALL)
    private List<SecondaryPermission> secondaryPermissions;

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public List<SecondaryPermission> getSecondaryPermissions() {
        return secondaryPermissions;
    }

    public void setSecondaryPermissions(List<SecondaryPermission> secondaryPermissions) {
        this.secondaryPermissions = secondaryPermissions;
    }
}
