package com.cleveron.permission.manager.demo.dto;

public class TertiaryPermissionDTO {
    private Boolean isActive = false;
    private Long parentPermissionId;

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Long getParentPermissionId() {
        return parentPermissionId;
    }

    public void setParentPermissionId(Long parentPermissionId) {
        this.parentPermissionId = parentPermissionId;
    }
}
