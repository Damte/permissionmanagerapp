package com.cleveron.permission.manager.demo.repository;

import com.cleveron.permission.manager.demo.entity.SecondaryPermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SecondaryPermissionRepository extends JpaRepository<SecondaryPermission, Long> {
    List<SecondaryPermission> findAllByIsActiveFalse();

}
