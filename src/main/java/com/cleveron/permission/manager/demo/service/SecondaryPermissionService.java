package com.cleveron.permission.manager.demo.service;

import com.cleveron.permission.manager.demo.dto.SecondaryPermissionDTO;
import com.cleveron.permission.manager.demo.entity.SecondaryPermission;
import com.cleveron.permission.manager.demo.exception.MissingParentPermission;

import java.util.List;

public interface SecondaryPermissionService {
    SecondaryPermission findById(Long id);

    SecondaryPermission saveSecondaryPermission(SecondaryPermission secondaryPermission);

    List<SecondaryPermission> findAllInactive();

    SecondaryPermission togglePermissionIsActive(Long permissionId, SecondaryPermissionDTO secondaryPermissionDTO) throws MissingParentPermission;
}
