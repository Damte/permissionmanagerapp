package com.cleveron.permission.manager.demo.exception;

public class PermissionNotIncludedInRole extends Throwable {
    public PermissionNotIncludedInRole(String s) {
        super(s);
    }

}
