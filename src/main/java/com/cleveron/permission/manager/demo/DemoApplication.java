package com.cleveron.permission.manager.demo;

import com.cleveron.permission.manager.demo.entity.*;
import com.cleveron.permission.manager.demo.repository.PrimaryPermissionRepository;
import com.cleveron.permission.manager.demo.repository.RoleRepository;
import com.cleveron.permission.manager.demo.repository.SecondaryPermissionRepository;
import com.cleveron.permission.manager.demo.repository.TertiaryPermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;
import java.util.Set;

@SpringBootApplication
@EnableSwagger2
public class DemoApplication implements CommandLineRunner {

    private final RoleRepository roleRepository;
    private final PrimaryPermissionRepository primaryPermissionRepository;
    private final SecondaryPermissionRepository secondaryPermissionRepository;
    private final TertiaryPermissionRepository tertiaryPermissionRepository;

    @Autowired
    public DemoApplication(RoleRepository roleRepository, PrimaryPermissionRepository primaryPermissionRepository, SecondaryPermissionRepository secondaryPermissionRepository, TertiaryPermissionRepository tertiaryPermissionRepository) {
        this.roleRepository = roleRepository;
        this.primaryPermissionRepository = primaryPermissionRepository;
        this.secondaryPermissionRepository = secondaryPermissionRepository;
        this.tertiaryPermissionRepository = tertiaryPermissionRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("com.cleveron.permission.manager.demo")).build();
    }

    @Override
    public void run(String... args) throws Exception {
        User user01 = new User();
        user01.setEmail("example@mail.com");
//		user.setId(1l);
        user01.setUsername("User-01");

        Role role01 = new Role();
        role01.setName("Basic user");
        role01.setUsers(Set.of(user01));

        Role role02 = new Role();
        role02.setName("Professional user");
        role02.setUsers(Set.of(user01));

        Role role03 = new Role();
        role03.setName("Administrator");
        role03.setUsers(Set.of(user01));

        PrimaryPermission primaryPermission01 = new PrimaryPermission();
        primaryPermission01.setName("create user profile");
//		primaryPermission01.setRoles(List.of(role01, role02));

        PrimaryPermission primaryPermission02 = new PrimaryPermission();
        primaryPermission02.setName("post parcel");
//		primaryPermission01.setRoles(List.of(role01, role02));

        PrimaryPermission primaryPermission03 = new PrimaryPermission();
        primaryPermission03.setName("track parcel");

        PrimaryPermission primaryPermission04 = new PrimaryPermission();
        primaryPermission04.setName("access to admin menu");

        SecondaryPermission secondaryPermission01_01 = new SecondaryPermission();
        SecondaryPermission secondaryPermission01_02 = new SecondaryPermission();
        SecondaryPermission secondaryPermission01_03 = new SecondaryPermission();
        SecondaryPermission secondaryPermission02_01 = new SecondaryPermission();
        SecondaryPermission secondaryPermission02_02 = new SecondaryPermission();
        SecondaryPermission secondaryPermission02_03 = new SecondaryPermission();
        SecondaryPermission secondaryPermission02_04 = new SecondaryPermission();
        SecondaryPermission secondaryPermission03_01 = new SecondaryPermission();
        SecondaryPermission secondaryPermission03_02 = new SecondaryPermission();
        SecondaryPermission secondaryPermission03_03 = new SecondaryPermission();
        SecondaryPermission secondaryPermission04_01 = new SecondaryPermission();
        SecondaryPermission secondaryPermission04_02 = new SecondaryPermission();
        SecondaryPermission secondaryPermission04_03 = new SecondaryPermission();
        secondaryPermission01_01.setName("personalize user profile");
        secondaryPermission01_01.setActive(true);
        secondaryPermission01_01.setPrimaryPermission(primaryPermission01);
        secondaryPermission01_02.setName("add multiple addresses");
        secondaryPermission01_02.setActive(true);
        secondaryPermission01_02.setPrimaryPermission(primaryPermission01);
        secondaryPermission01_03.setName("set secondary profile options");
        secondaryPermission01_03.setActive(false);
        secondaryPermission01_03.setPrimaryPermission(primaryPermission01);
        secondaryPermission02_01.setName("send multiple parcels simultaneously");
        secondaryPermission02_01.setActive(false);
        secondaryPermission02_01.setPrimaryPermission(primaryPermission02);
        secondaryPermission02_02.setName("set delivery date");
        secondaryPermission02_02.setActive(true);
        secondaryPermission02_02.setPrimaryPermission(primaryPermission02);
        secondaryPermission02_03.setName("set priority level");
        secondaryPermission02_03.setActive(true);
        secondaryPermission02_03.setPrimaryPermission(primaryPermission02);
        secondaryPermission02_04.setName("recall the parcel");
        secondaryPermission02_04.setActive(false);
        secondaryPermission02_04.setPrimaryPermission(primaryPermission02);
        secondaryPermission03_01.setName("track delivery");
        secondaryPermission03_01.setActive(true);
        secondaryPermission03_01.setPrimaryPermission(primaryPermission03);
        secondaryPermission03_02.setName("keep parcel history");
        secondaryPermission03_02.setActive(true);
        secondaryPermission03_02.setPrimaryPermission(primaryPermission03);
//		secondaryPermission03_03.setName("live tracking");
//		secondaryPermission03_03.setActive(true);
//		secondaryPermission03_03.setPrimaryPermission(primaryPermission03);
        secondaryPermission04_01.setName("edit users");
        secondaryPermission04_01.setActive(true);
        secondaryPermission04_01.setPrimaryPermission(primaryPermission04);
        secondaryPermission04_02.setName("edit delivery details");
        secondaryPermission04_02.setActive(true);
        secondaryPermission04_02.setPrimaryPermission(primaryPermission04);
        secondaryPermission04_03.setName("cancel delivery");
        secondaryPermission04_03.setActive(false);
        secondaryPermission04_03.setPrimaryPermission(primaryPermission04);

        TertiaryPermission tertiaryPermission01_01_01 = new TertiaryPermission();
        tertiaryPermission01_01_01.setName("delete profile");
        tertiaryPermission01_01_01.setActive(true);
        tertiaryPermission01_01_01.setSecondaryPermission(secondaryPermission01_01);
        TertiaryPermission tertiaryPermission01_01_02 = new TertiaryPermission();
        tertiaryPermission01_01_02.setName("add sections");
        tertiaryPermission01_01_02.setSecondaryPermission(secondaryPermission01_01);
        TertiaryPermission tertiaryPermission01_01_03 = new TertiaryPermission();
        tertiaryPermission01_01_03.setName("view other users profile");
        tertiaryPermission01_01_03.setSecondaryPermission(secondaryPermission01_01);
        TertiaryPermission tertiaryPermission01_02_01 = new TertiaryPermission();
        tertiaryPermission01_02_01.setName("delete address");
        tertiaryPermission01_02_01.setActive(true);
        tertiaryPermission01_02_01.setSecondaryPermission(secondaryPermission01_02);
        TertiaryPermission tertiaryPermission01_02_02 = new TertiaryPermission();
        tertiaryPermission01_02_02.setName("set international address");
        tertiaryPermission01_02_02.setSecondaryPermission(secondaryPermission01_02);
        TertiaryPermission tertiaryPermission01_03_01 = new TertiaryPermission();
        tertiaryPermission01_03_01.setName("send direct messages to other users");
        tertiaryPermission01_03_01.setActive(true);
        tertiaryPermission01_03_01.setSecondaryPermission(secondaryPermission01_03);
        TertiaryPermission tertiaryPermission01_03_02 = new TertiaryPermission();
        tertiaryPermission01_03_02.setName("add family members");
        tertiaryPermission01_03_02.setSecondaryPermission(secondaryPermission01_03);


        TertiaryPermission tertiaryPermission02_01_01 = new TertiaryPermission();
        tertiaryPermission02_01_01.setName("group multiple parcels in one same package");
        tertiaryPermission02_01_01.setActive(true);
        tertiaryPermission02_01_01.setSecondaryPermission(secondaryPermission02_01);
        TertiaryPermission tertiaryPermission02_01_02 = new TertiaryPermission();
        tertiaryPermission02_01_02.setName("add special wrapping");
        tertiaryPermission02_01_02.setSecondaryPermission(secondaryPermission02_01);
        TertiaryPermission tertiaryPermission02_01_03 = new TertiaryPermission();
        tertiaryPermission02_01_03.setName("include personal message");
        tertiaryPermission02_01_03.setActive(true);
        tertiaryPermission02_01_03.setSecondaryPermission(secondaryPermission02_01);
        TertiaryPermission tertiaryPermission02_02_01 = new TertiaryPermission();
        tertiaryPermission02_02_01.setName("set time constraints for delivery");
        tertiaryPermission02_02_01.setActive(true);
        tertiaryPermission02_02_01.setSecondaryPermission(secondaryPermission02_02);
        TertiaryPermission tertiaryPermission02_02_02 = new TertiaryPermission();
        tertiaryPermission02_02_02.setName("set pre-warning for recipient");
        tertiaryPermission02_02_02.setSecondaryPermission(secondaryPermission02_02);

        TertiaryPermission tertiaryPermission03_01_01 = new TertiaryPermission();
        tertiaryPermission03_01_01.setName("track status");
        tertiaryPermission03_01_01.setActive(true);
        tertiaryPermission03_01_01.setSecondaryPermission(secondaryPermission03_01);
        TertiaryPermission tertiaryPermission03_01_02 = new TertiaryPermission();
        tertiaryPermission03_01_02.setName("live tracking");
        tertiaryPermission03_01_02.setSecondaryPermission(secondaryPermission03_01);
        TertiaryPermission tertiaryPermission03_02_01 = new TertiaryPermission();
        tertiaryPermission03_02_01.setName("delete item history");
        tertiaryPermission03_02_01.setActive(true);
        tertiaryPermission03_02_01.setSecondaryPermission(secondaryPermission03_02);
        TertiaryPermission tertiaryPermission03_02_02 = new TertiaryPermission();
        tertiaryPermission03_02_02.setName("delete whole history");
        tertiaryPermission03_02_02.setSecondaryPermission(secondaryPermission03_02);

        TertiaryPermission tertiaryPermission04_01_01 = new TertiaryPermission();
        tertiaryPermission04_01_01.setName("disable user");
        tertiaryPermission04_01_01.setActive(true);
        tertiaryPermission04_01_01.setSecondaryPermission(secondaryPermission04_01);
        TertiaryPermission tertiaryPermission04_01_02 = new TertiaryPermission();
        tertiaryPermission04_01_02.setActive(true);
        tertiaryPermission04_01_02.setName("delete user");
        tertiaryPermission04_01_02.setSecondaryPermission(secondaryPermission04_01);
        TertiaryPermission tertiaryPermission04_01_03 = new TertiaryPermission();
        tertiaryPermission04_01_03.setName("upgrade user");
        tertiaryPermission04_01_03.setSecondaryPermission(secondaryPermission04_01);
        TertiaryPermission tertiaryPermission04_02_01 = new TertiaryPermission();
        tertiaryPermission04_02_01.setName("contact user");
        tertiaryPermission04_02_01.setActive(true);
        tertiaryPermission04_02_01.setSecondaryPermission(secondaryPermission04_02);
        TertiaryPermission tertiaryPermission04_02_02 = new TertiaryPermission();
        tertiaryPermission04_02_02.setActive(true);
        tertiaryPermission04_02_02.setName("modify priority");
        tertiaryPermission04_02_02.setSecondaryPermission(secondaryPermission04_02);
        TertiaryPermission tertiaryPermission04_03_01 = new TertiaryPermission();
        tertiaryPermission04_03_01.setName("disable address");
        tertiaryPermission04_03_01.setActive(true);
        tertiaryPermission04_03_01.setSecondaryPermission(secondaryPermission04_03);
        TertiaryPermission tertiaryPermission04_03_02 = new TertiaryPermission();
        tertiaryPermission04_03_02.setActive(true);
        tertiaryPermission04_03_02.setName("return parcel");
        tertiaryPermission04_03_02.setSecondaryPermission(secondaryPermission04_03);


        primaryPermission01.setSecondaryPermissions(List.of(secondaryPermission01_01, secondaryPermission01_02, secondaryPermission01_03));
        role01.setPermissions(List.of(primaryPermission01, primaryPermission02));
        role02.setPermissions(List.of(primaryPermission01, primaryPermission02, primaryPermission03));
        role03.setPermissions(List.of(primaryPermission04));

        primaryPermissionRepository.save(primaryPermission01);
        primaryPermissionRepository.save(primaryPermission02);
        primaryPermissionRepository.save(primaryPermission03);
        primaryPermissionRepository.save(primaryPermission04);
        secondaryPermissionRepository.save(secondaryPermission01_01);
        secondaryPermissionRepository.save(secondaryPermission01_02);
        secondaryPermissionRepository.save(secondaryPermission01_03);
        secondaryPermissionRepository.save(secondaryPermission02_01);
        secondaryPermissionRepository.save(secondaryPermission02_02);
        secondaryPermissionRepository.save(secondaryPermission02_03);
        secondaryPermissionRepository.save(secondaryPermission02_04);
        secondaryPermissionRepository.save(secondaryPermission03_01);
        secondaryPermissionRepository.save(secondaryPermission03_02);
//		secondaryPermissionRepository.save(secondaryPermission03_03);
        secondaryPermissionRepository.save(secondaryPermission04_01);
        secondaryPermissionRepository.save(secondaryPermission04_02);
        secondaryPermissionRepository.save(secondaryPermission04_03);
        roleRepository.save(role01);
        roleRepository.save(role02);
        roleRepository.save(role03);
        tertiaryPermissionRepository.save(tertiaryPermission01_01_01);
        tertiaryPermissionRepository.save(tertiaryPermission01_01_02);
        tertiaryPermissionRepository.save(tertiaryPermission01_01_03);
        tertiaryPermissionRepository.save(tertiaryPermission01_02_01);
        tertiaryPermissionRepository.save(tertiaryPermission01_02_02);
        tertiaryPermissionRepository.save(tertiaryPermission01_03_01);
        tertiaryPermissionRepository.save(tertiaryPermission01_03_02);
        tertiaryPermissionRepository.save(tertiaryPermission02_01_01);
        tertiaryPermissionRepository.save(tertiaryPermission02_01_02);
        tertiaryPermissionRepository.save(tertiaryPermission02_01_03);
        tertiaryPermissionRepository.save(tertiaryPermission02_02_01);
        tertiaryPermissionRepository.save(tertiaryPermission02_02_02);
        tertiaryPermissionRepository.save(tertiaryPermission03_01_01);
        tertiaryPermissionRepository.save(tertiaryPermission03_01_02);
        tertiaryPermissionRepository.save(tertiaryPermission03_02_01);
        tertiaryPermissionRepository.save(tertiaryPermission03_02_02);
        tertiaryPermissionRepository.save(tertiaryPermission04_01_01);
        tertiaryPermissionRepository.save(tertiaryPermission04_01_02);
        tertiaryPermissionRepository.save(tertiaryPermission04_01_03);
        tertiaryPermissionRepository.save(tertiaryPermission04_02_01);
        tertiaryPermissionRepository.save(tertiaryPermission04_02_02);
        tertiaryPermissionRepository.save(tertiaryPermission04_03_01);
        tertiaryPermissionRepository.save(tertiaryPermission04_03_02);
        System.out.println("Application started");
    }
}
