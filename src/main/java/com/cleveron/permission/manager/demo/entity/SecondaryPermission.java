package com.cleveron.permission.manager.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class SecondaryPermission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "primary_permission_id")
    private PrimaryPermission primaryPermission;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "secondaryPermission", cascade = CascadeType.ALL)
    private List<TertiaryPermission> tertiaryPermissions;

    private Boolean isActive = false;


    public Long getId() {
        return id;
    }

    public void setId(Long entityId) {
        this.id = entityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PrimaryPermission getPrimaryPermission() {
        return primaryPermission;
    }

    public void setPrimaryPermission(PrimaryPermission primaryPermission) {
        this.primaryPermission = primaryPermission;
    }

    public List<TertiaryPermission> getTertiaryPermissions() {
        return tertiaryPermissions;
    }

    public void setTertiaryPermissions(List<TertiaryPermission> tertiaryPermissions) {
        this.tertiaryPermissions = tertiaryPermissions;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}


