package com.cleveron.permission.manager.demo.service.serviceImpl;

import com.cleveron.permission.manager.demo.dto.TertiaryPermissionDTO;
import com.cleveron.permission.manager.demo.entity.TertiaryPermission;
import com.cleveron.permission.manager.demo.exception.MissingParentPermission;
import com.cleveron.permission.manager.demo.repository.TertiaryPermissionRepository;
import com.cleveron.permission.manager.demo.service.SecondaryPermissionService;
import com.cleveron.permission.manager.demo.service.TertiaryPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TertiaryPermissionServiceImpl implements TertiaryPermissionService {

    private final TertiaryPermissionRepository tertiaryPermissionRepository;
    private final SecondaryPermissionService secondaryPermissionService;

    @Autowired
    public TertiaryPermissionServiceImpl(TertiaryPermissionRepository tertiaryPermissionRepository, SecondaryPermissionService secondaryPermissionService) {
        this.tertiaryPermissionRepository = tertiaryPermissionRepository;
        this.secondaryPermissionService = secondaryPermissionService;
    }

    @Override
    public TertiaryPermission saveTertiaryPermission(TertiaryPermission tertiaryPermission) {
        return tertiaryPermissionRepository.save(tertiaryPermission);
    }

    @Override
    public List<TertiaryPermission> findAll() {
        return tertiaryPermissionRepository.findAll();
    }

    @Override
    public TertiaryPermission togglePermissionIsActive(Long permissionId, TertiaryPermissionDTO tertiaryPermissionDTO) throws MissingParentPermission {
        TertiaryPermission permissionToBeUpdated = tertiaryPermissionRepository.findById(permissionId).get();
        if (secondaryPermissionService.findById(tertiaryPermissionDTO.getParentPermissionId()).getTertiaryPermissions().contains(permissionToBeUpdated)) {
            permissionToBeUpdated.setActive(tertiaryPermissionDTO.getActive());
            return tertiaryPermissionRepository.save(permissionToBeUpdated);
        } else {
            throw new MissingParentPermission("Please add the parent permission first: " + permissionToBeUpdated.getSecondaryPermission().getName());
        }
    }
}
