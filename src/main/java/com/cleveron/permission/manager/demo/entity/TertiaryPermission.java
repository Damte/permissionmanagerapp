package com.cleveron.permission.manager.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class TertiaryPermission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "secondary_permission_id")
    private SecondaryPermission secondaryPermission;

    private Boolean isActive = false;


    public Long getId() {
        return id;
    }

    public void setId(Long entityId) {
        this.id = entityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SecondaryPermission getSecondaryPermission() {
        return secondaryPermission;
    }

    public void setSecondaryPermission(SecondaryPermission secondaryPermission) {
        this.secondaryPermission = secondaryPermission;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
