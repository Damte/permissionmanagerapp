package com.cleveron.permission.manager.demo.service;

import com.cleveron.permission.manager.demo.entity.PrimaryPermission;

import java.util.List;

public interface PrimaryPermissionService {
    PrimaryPermission savePermission(PrimaryPermission primaryPermission);

    List<PrimaryPermission> findAll();

    PrimaryPermission findById(Long id);
}
