package com.cleveron.permission.manager.demo.dto;

public class SecondaryPermissionDTO {
    private Boolean isActive = false;
    private Long primaryPermissionId;

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Long getPrimaryPermissionId() {
        return primaryPermissionId;
    }

    public void setPrimaryPermissionId(Long primaryPermissionId) {
        this.primaryPermissionId = primaryPermissionId;
    }
}
