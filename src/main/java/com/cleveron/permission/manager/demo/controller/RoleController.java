package com.cleveron.permission.manager.demo.controller;

import com.cleveron.permission.manager.demo.dto.PrimaryPermissionDTO;
import com.cleveron.permission.manager.demo.entity.PrimaryPermission;
import com.cleveron.permission.manager.demo.entity.Role;
import com.cleveron.permission.manager.demo.exception.PermissionAlreadyExistsOnRole;
import com.cleveron.permission.manager.demo.exception.PermissionNotIncludedInRole;
import com.cleveron.permission.manager.demo.exception.RoleDoesntExist;
import com.cleveron.permission.manager.demo.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class RoleController {
    private RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @PatchMapping(value = "role/{id}")
    public ResponseEntity<Role> updatePermissionsForRole(@PathVariable("id") Long roleId, @RequestBody List<PrimaryPermission> primaryPermissions) {
        Role updatedRole = roleService.updatePermission(roleId, primaryPermissions);
        return new ResponseEntity<>(updatedRole, HttpStatus.ACCEPTED);
    }

    @PutMapping(value = "role/{id}")
    public ResponseEntity<Role> updateRole(@PathVariable("id") Long roleId, @RequestBody Role newRole) {
        Role existingRole = roleService.findRole(roleId);
        existingRole.setName(newRole.getName());
        existingRole.setPermissions(newRole.getPermissions());
        Role updatedRole = roleService.updateRole(existingRole);
        return new ResponseEntity<>(updatedRole, HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/roles")
    public ResponseEntity<List<Role>> getAll() {
        try {
            List<Role> roles = roleService.findAllRoles();
            return new ResponseEntity(roles, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "role/{role-id}/primary-permissions")
    public ResponseEntity<List<PrimaryPermission>> getPermissionsByRole(@PathVariable("role-id") Long roleId) {
        try {
            if (roleService.checkRoleExistence(roleId)) {
                List<PrimaryPermission> primaryPermissions = roleService.findPermissionsByRoleId(roleId);
                return new ResponseEntity(primaryPermissions, HttpStatus.FOUND);
            } else {
                throw new RoleDoesntExist("There is no role with Id: " + roleId);
            }
        } catch (RoleDoesntExist e) {
            return new ResponseEntity(e, HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping(value = "/role/{role-id}/primary-permission")
    public ResponseEntity<List<PrimaryPermission>> addPermissionToRole(@PathVariable("role-id") Long roleId, @RequestBody PrimaryPermissionDTO primaryPermission) {
        try {
            List<PrimaryPermission> updateListOfPermissions = roleService.addNewPermission(roleId, primaryPermission);
            return new ResponseEntity<>(updateListOfPermissions, HttpStatus.ACCEPTED);
        } catch (PermissionAlreadyExistsOnRole e) {
            return new ResponseEntity(e, HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping(value = "/role/{role-id}/primary-permission")
    public ResponseEntity<List<PrimaryPermission>> removePermissionToRole(@PathVariable("role-id") Long roleId, @RequestBody PrimaryPermissionDTO primaryPermission) {
        try {
            List<PrimaryPermission> updateListOfPermissions = roleService.removeExistingPermission(roleId, primaryPermission);
            return new ResponseEntity<>(updateListOfPermissions, HttpStatus.ACCEPTED);
        } catch (PermissionNotIncludedInRole e) {
            return new ResponseEntity(e, HttpStatus.NOT_FOUND);
        }
    }


}
