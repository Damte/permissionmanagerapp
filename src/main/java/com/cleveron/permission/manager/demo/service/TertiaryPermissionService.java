package com.cleveron.permission.manager.demo.service;

import com.cleveron.permission.manager.demo.dto.TertiaryPermissionDTO;
import com.cleveron.permission.manager.demo.entity.TertiaryPermission;
import com.cleveron.permission.manager.demo.exception.MissingParentPermission;

import java.util.List;

public interface TertiaryPermissionService {
    TertiaryPermission saveTertiaryPermission(TertiaryPermission tertiaryPermission);

    List<TertiaryPermission> findAll();

    TertiaryPermission togglePermissionIsActive(Long parentPermissionId, TertiaryPermissionDTO tertiaryPermissionDTO) throws MissingParentPermission;
}
