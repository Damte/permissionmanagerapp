package com.cleveron.permission.manager.demo.controller;

import com.cleveron.permission.manager.demo.dto.SecondaryPermissionDTO;
import com.cleveron.permission.manager.demo.entity.SecondaryPermission;
import com.cleveron.permission.manager.demo.exception.MissingParentPermission;
import com.cleveron.permission.manager.demo.service.SecondaryPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class SecondaryPermissionController {

    private SecondaryPermissionService secondaryPermissionService;

    @Autowired
    public SecondaryPermissionController(SecondaryPermissionService secondaryPermissionService) {
        this.secondaryPermissionService = secondaryPermissionService;
    }

    @GetMapping(value = "/secondary-permissions")
    public ResponseEntity<List<SecondaryPermission>> findAllPrimaryPermissions() {
        List<SecondaryPermission> secondaryPermissions = secondaryPermissionService.findAllInactive();
        return new ResponseEntity<>(secondaryPermissions, HttpStatus.OK);
    }

    @PatchMapping(value = "/secondary-permission/{id}")
    public ResponseEntity<SecondaryPermission> togglePermissionIsActive(@PathVariable("id") Long roleId, @RequestBody SecondaryPermissionDTO secondaryPermissionDTO) {
        try {
            SecondaryPermission updatePermission = secondaryPermissionService.togglePermissionIsActive(roleId, secondaryPermissionDTO);
            return new ResponseEntity<>(updatePermission, HttpStatus.ACCEPTED);
        } catch (MissingParentPermission e) {
            return new ResponseEntity(e, HttpStatus.CONFLICT);
        }
    }

}
