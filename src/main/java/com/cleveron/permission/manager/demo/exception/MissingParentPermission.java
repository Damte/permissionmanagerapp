package com.cleveron.permission.manager.demo.exception;

public class MissingParentPermission extends Throwable {
    public MissingParentPermission(String s) {
        super(s);
    }

}
