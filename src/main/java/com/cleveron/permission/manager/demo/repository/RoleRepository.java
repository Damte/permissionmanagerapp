package com.cleveron.permission.manager.demo.repository;

import com.cleveron.permission.manager.demo.entity.PrimaryPermission;
import com.cleveron.permission.manager.demo.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    List<PrimaryPermission> findAllByRoleId(Long roleId);
}
