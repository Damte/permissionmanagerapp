package com.cleveron.permission.manager.demo.repository;

import com.cleveron.permission.manager.demo.entity.PrimaryPermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrimaryPermissionRepository extends JpaRepository<PrimaryPermission, Long> {

}
